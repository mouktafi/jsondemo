package com.app.jsondemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.jsondemo.R;
import com.app.jsondemo.model.Attrazione;

import java.util.ArrayList;
import java.util.List;



public class AttrazioniAdapter extends ArrayAdapter<Attrazione> {
    private final ArrayList<Attrazione> attrazioni;
    private final LayoutInflater inflater;
    private final int Resource;
    Context context;

    public AttrazioniAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Attrazione> objects) {
        super(context, resource, objects);

        this.context = context;
        this.Resource = resource;
        this.attrazioni = objects;
        inflater = (LayoutInflater.from(context));
    }

    private static class ViewHolder {
        TextView nome;
        TextView tipo;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        ViewHolder viewHolder;

        if(convertView == null){
            convertView = inflater.inflate(Resource, null);
            viewHolder = new ViewHolder();
            viewHolder.nome = (TextView)convertView.findViewById(R.id.tv_name);
            viewHolder.tipo = (TextView)convertView.findViewById(R.id.tv_type);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }
        viewHolder.nome.setText(attrazioni.get(position).getName());
        viewHolder.tipo.setText(attrazioni.get(position).getType());

        return convertView;
    }
}
