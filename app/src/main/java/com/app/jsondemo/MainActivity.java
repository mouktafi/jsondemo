package com.app.jsondemo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.app.jsondemo.adapter.AttrazioniAdapter;
import com.app.jsondemo.model.Attrazione;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Attrazione> attrazioni;
    AttrazioniAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        attrazioni = new ArrayList<Attrazione>();
        ListView listView = (ListView)findViewById(R.id.list_attrazioni);

        DownloadTask task = new DownloadTask();
        task.execute("http://appacademy.it/book/attrazioni.json");

        adapter = new AttrazioniAdapter(getApplicationContext(),R.layout.item_attrazione, attrazioni);
        listView.setAdapter(adapter);
    }
    public class DownloadTask extends AsyncTask<String, Void, String> {

        private ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(MainActivity.this);
            dialog.setMessage("Caricamento");
            dialog.setTitle("Connessione");
            dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... strings) {

            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;

            try{
                url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);

                int data = reader.read();
                while (data != -1) {
                    char cur = (char)data;
                    result += cur;
                    data = reader.read();
                }
                return result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //Log.i("JSONDemo", result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                String attrazioni_str = jsonObject.getString("attractions");
                //Log.i("JSONDemo", attrazioni_str);
                JSONArray array = new JSONArray(attrazioni_str);

                for(int i = 0; i<array.length(); i++){

                    Attrazione att = new Attrazione();
                    JSONObject jsonPart = array.getJSONObject(i);
                    String nomeAtt = jsonPart.getString("name");
                    String tipoAtt = jsonPart.getString("type");
                    String latAtt = jsonPart.getString("latitude");
                    String longAtt = jsonPart.getString("longitude");

                    att.setName(nomeAtt);
                    att.setType(tipoAtt);
                    attrazioni.add(att);

                    Log.i("JSONDemo", nomeAtt + " " + tipoAtt + " " + latAtt + " " +longAtt);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            dialog.cancel();
            adapter.notifyDataSetChanged();

        }
    }
}